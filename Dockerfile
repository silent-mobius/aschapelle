FROM python:3.11-alpine AS base
COPY ./pyproject.toml ./
RUN apk add --no-cache --update build-base git python3-dev
RUN pip install poetry
RUN poetry config virtualenvs.create false
RUN poetry install --no-dev

FROM python:3.11-alpine
LABEL MAINTAINER=Silen-Mobius VERSION=${VERSION}
WORKDIR /home/aschapelle
RUN apk add git
COPY --from=base /usr/local/lib/python3.11/site-packages /usr/local/lib/python3.11/site-packages 
COPY --from=base /usr/local/bin /usr/local/bin
COPY . /home/aschapelle/
EXPOSE 8000
EXPOSE 465
CMD [ "gunicorn", "--config", "/home/aschapelle/gunicorn_conf.py", "src.aschapelle:app" ]
# gunicorn --config /home/aschapelle/gunicorn_conf.py src.aschapelle:app