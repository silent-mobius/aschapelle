import pytest
from src.aschapelle import app as flask_app


@pytest.fixture
def app():
    yield flask_app

@pytest.fixture
def client(app):
    app.config.DEBUG = True
    return app.test_client()
