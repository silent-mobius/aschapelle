import json


def test_slash(app, client):
    response = client.get('/')
    assert response.status_code == 200

def test_index(app,client):
    response = client.get('/index')
    assert response.status_code == 200

def test_index_html(app, client):
    response = client.get('/index.html')
    assert response.status_code == 200


def test_robots(app, client):
    response = client.get('/robots')
    assert response.status_code == 404

def test_robots_txt(app, client):
    response = client.get('/robots.txt')
    assert response.status_code == 200

def test_sitemap(app, client):
    response = client.get('/sitemap')
    assert response.status_code == 404

def test_sitemap_xml(app, client):
    response = client.get('/sitemap.xml')
    assert response.status_code == 200

def test_404(app, client):
    response = client.get('/abrakadbra')
    assert response.status_code == 404