# Just my site

My personal site build upon Python3 with :
- Flask
- SQLAlchemy
- TinyDB
- Pytest
- Pylint
- Pipenv
- Docker
- Docker-coompose
- Gitlab-CI

## Purpose:
Practice python3 developement, while working on markdown and jinja2 transformation, sending email, json based database in combitnation of mysql.

Whole project is saved in `python library structure`, tested with `pytest` and `pylint` while running in CI with `gitlab-ci` but also seperated into micro-services with mutliple docker images and deployed to remote server with docker-compose remote host connection. 

## Project Structure

```sh
aschapelle/
├── aschapelle.py
├── docker-compose.yml
├── Dockerfile
├── gunicorn_conf.py
├── logs
│   ├── access.log
│   └── error.log
├── manifest.yaml
├── Pipfile
├── pyproject.toml
├── README.md
├── spell.txt
├── src
│   ├── aschapelle
│   │   ├── config.py
│   │   ├── __init__.py
│   │   ├── libs
│   │   │   ├── __init__.py
│   │   │   └── models.py
│   │   ├── route.py
│   │   ├── static
│   │   │   ├── assets
│   │   │   │   └── img
│   │   │   │       ├── favicon.ico
│   │   │   │       └── profile.jpg
│   │   │   ├── css
│   │   │   │   └── styles.css
│   │   │   ├── js
│   │   │   │   └── scripts.js
│   │   │   ├── robots.txt
│   │   │   └── sitemap.xml
│   │   └── templates
│   │       ├── 404.html
│   │       ├── 500.html
│   │       ├── about.html
│   │       ├── awards.html
│   │       ├── education.html
│   │       ├── experience.html
│   │       ├── index.html
│   │       ├── interests.html
│   │       ├── layout.html
│   │       ├── mail.html
│   │       ├── nav.html
│   │       ├── projects.html
│   │       └── skills.html
│   └── requirements.txt
└── tests
    ├── conftest.py
    └── test_app.py
```

## Usage

To use the project create virtual env, i suggest to use `pipenv`, install requirements.txt dependencies, and run the `gunicorn` with its config file

example below:
```sh
sudo apt-get/yum/dnf install -y python3 python3-pip pipenv
cd /path/to/aschapelle/project
pipenv shell # this will create virtual environment
pip install -r src/requirements.txt
gunicorn --config gunicorn_conf.py src.aschapelle:app
# Go over gunicorn-conf to see the defined values
```

## Developement

In case you are interested in adding to fuctionality, fixing bugs or anything else:
 - fork the repo
 - do the change 
 - push to branch named `DEVEL`  and we'll test all in CI and deploy `beta.aschapelle.com` for live system to debug on.




Handy Links:

- [this is handy](https://gardenvariety.medium.com/easy-https-with-letsencrypt-and-docker-compose-168df411e2d2)
