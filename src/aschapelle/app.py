import os
from flask import Flask
from redis import Redis
from flask import render_template, redirect, request
from flask import send_from_directory
from flask_mail import Mail
from flask_sqlalchemy import SQLAlchemy
from src.aschapelle.libs.models import UserForm
from src.aschapelle.libs.utils import send_mail, get_version, validate_mail
from src.aschapelle.libs.utils import wait_for_service

app = Flask(__name__)
app.config.from_pyfile('config.py')
mail = Mail(app)

wait_for_service('redis')
wait_for_service('postgre')

db = SQLAlchemy(app)
cache  = Redis(host='redis-server', port=6379, decode_responses=True)
version = get_version()

@app.route('/', methods=['GET', 'POST'])
@app.route('/index', methods=['GET', 'POST'])
@app.route('/index.html',methods=['GET', 'POST'])
def index():
    form = UserForm()
    if request.method == 'POST':
        contact_info = {'user':request.form.get('username'), \
                        'usermail':request.form.get('usermail'), \
                        'usercell':request.form.get('usermsg'), \
                        'usermsg': request.form.get('usercell')
                        }
        mail_validation = validate_mail(contact_info)
        if mail_validation:
            send_mail(mail, str(contact_info))
            return redirect('/index')
        return redirect('/index#about')
    return render_template('index.html', title='Home', form=form, version=version)

@app.route('/favicon.ico')
def favicon():
    # print(app.static_folder,request.path[1:])
    return send_from_directory(os.path.join(app.root_path,'static/assets/img/'), request.path[1:])

@app.route('/robots')
@app.route('/robots.txt')
@app.route('/sitemap')
@app.route('/sitemap.xml')
def robots():
    return send_from_directory(app.static_folder, request.path[1:])

@app.errorhandler(404)
def page_not_found(e_error):
    print(e_error)
    return render_template('404.html'), 404

@app.errorhandler(500)
def page_error(e_error):
    print(e_error)
    return render_template('500.html'), 500
