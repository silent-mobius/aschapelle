'''
config file for app
'''
import os

SECRET_KEY = os.environ['ACCESS'] or '!!5S0m#wh$tVERYL0nGQWERTYpa$$W0d'
MAIL_SERVER=os.environ['MAIL_SERVER']
MAIL_PORT = os.environ['MAIL_PORT']
MAIL_USERNAME = os.environ['MAIL_USER']
MAIL_PASSWORD = os.environ['MAIL_SECRET']
MAIL_USE_SSL= os.environ['MAIL_USE_SSL']
SQLALCHEMY_DATABASE_URI=os.environ['SQLALCHEMY_DATABASE_URI']
