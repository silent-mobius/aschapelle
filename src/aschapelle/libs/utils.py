import os
import json
import time
import random
import platform
import subprocess
import tomli
import tomli_w
from markdown import markdown
from flask_mail import Message


PWD=os.getcwd()


def gen_menu_table_to_use_from_cache():
    root_folder = os.getcwd()
    md_path_dict = {}
    for root_dir,_,_ in os.walk(root_folder):
        if 'md' in root_dir:
            md_path_dict[root_dir.split('/')[-1]] = root_dir

    return md_path_dict


def md_gen(path):
    path = str(path) + '/README.md'
    with open(path,encoding='utf-8') as mark_down:
        md_content = markdown(mark_down.read(), extensions=['fenced_code','codehilite',
                                                            'extra', 'toc', 'smarty',
                                                            'sane_lists', 'meta'])
    return md_content


def get_version():
    if not os.environ['VERSION']:
        try:
            with open('pyproject.toml',"rb") as toml:
                toml_dict = tomli.load(toml)
            return toml_dict['version']['current_version']
        except KeyError:
            print('[!] GET-VERSION: Something is wrong with pyproject.toml file')
            return False
    return os.environ['VERSION']

def send_mail(mail_obj, msg):
    try:
        message = Message('From VaioLabs.io: ',
                        sender='app@vaiolabs.com',
                        recipients = ['alex@vaiolabs.com'])
        message.body = msg
        mail_obj.send(message)
    except ConnectionRefusedError:
        pass
    except SMTPServerDisconnected:
        pass

def validate_mail(kwargs):
    mail_dict = {}
    for key,value in kwargs.items():
            if key is not None:
                mail_dict[key] = value
    if len(mail_dict) > 3:
        return mail_dict
    return False

def check_service_status(service):
    param = '-n' if platform.system().lower()=='windows' else '-c'
    command = ['ping', param, '1', service]

    return subprocess.call(command) == 0

def wait_for_service(service):
    while not check_service_status(service):
        print(f'{service} not available')
        time.sleep(2)


 
