from datetime import datetime

from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired

from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy.orm import declarative_base, sessionmaker

Base = declarative_base()

class ConnectionModel(Base):
    __tablename__ = 'connections'

    id = Column(String, primary_key=True )
    last_connection = Column(DateTime, default=datetime.utcnow)
    connection_count = Column(Integer, nullable=False)

class UserForm(FlaskForm):
    username = StringField('Name', validators=[DataRequired()])
    usermail = StringField('Email', validators=[DataRequired()])
    usermsg  = StringField('Message', validators=[DataRequired()])
    usercell = StringField('Phone')
    submit = SubmitField('Send')
